//const express = require('express')
import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import { connectDb } from './models';
import router from './routes/routes';

const app = express();
app.use(express.json());

const port = 3000
dotenv.config();

app.use(cors());

app.get('/', (req, res) => {
  res.send('Api hulk :)')
})

app.use("/api", router);

connectDb().then(async () => {
    console.log('Database connected sucessfully')
    app.listen(process.env.PORT || port, () => {
        console.log(`Server listening at http://localhost:${process.env.PORT || port}`)
    });
});


export default app;